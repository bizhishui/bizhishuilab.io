---
title: links
layout: page
---

#### Univ. & Institutes
- [北航](http://www.buaa.edu.cn/)
- [Centrale Marseille](https://www.centrale-marseille.fr/)


#### Labs
- [M2P2](http://www.m2p2.fr/)
- [The II. Institute for Theoretical Physics](https://www.itp2.uni-stuttgart.de/index.html)
- [LiPhy](https://www-liphy.ujf-grenoble.fr/-dyfcom-?lang=fr)
- [Nonlinear Physics and Fluid Dynamics Lab](http://www.weizmann.ac.il/complex/steinberg/): biological flow exp.
- [crunch group](https://www.brown.edu/research/projects/crunch/): Math + Machine Learning + X


#### Peoples
- [王志坚](http://zjwang.com/)
- [C. Pozrikidis](http://dehesa.freeshell.org/): 边界积分方法
- [Eric Lauga](http://www.damtp.cam.ac.uk/user/lauga/index.html): bio-swimmer
- [Anna-Karin Tornberg](https://www.kth.se/profile/akto): FMM, BIM, quadrature
- [朱来来](http://www.lailaiflow.com/index.html)
- [Jonathan B. Freund](http://jbfreund.mechse.illinois.edu/index.html)
- [Petia M. Vlahovska](http://people.esam.northwestern.edu/~vlahovska/index.html)
- [Lorena A. Barba](http://lorenabarba.com/): open source project
- [	Dmitry A. Fedosov](http://iffwww.iff.kfa-juelich.de/~fedosov/D_Fedosov.html)


#### Interesting Sites
- [鳥哥的 Linux 私房菜](http://linux.vbird.org/home.php)
- [古诗文网](http://www.gushiwen.org/)
- [比特之理](http://www.kylen314.com/)
- [考据癖](http://localhost-8080.com/) & [matrix67](http://www.matrix67.com/): 极客伉俪
- [UbuntuHandbook](http://ubuntuhandbook.org/): Ubuntu各种使用心得
- [宇宙的心弦](https://www.physixfan.com/)


#### Blogs
- [谢益辉](http://yihui.name/): 个人博客, R
- [PChou](http://www.pchou.info/index.html): Github page, Html5
- [Beiyuu](http://beiyuu.com/why-blog): 我为什么写博客
- [Steve losh](http://stevelosh.com/): math and game programming
- [Dominic Walliman](http://dominicwalliman.com/): 科学文化传播(science writer and physicist)
- [阮一峰的网络日志](http://www.ruanyifeng.com/home.html)
- [当然我在扯淡](http://www.yinwang.org/)
